<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class FileController extends BaseController
{
    public function procesarArchivo(Request $request)
    {
        $content = "";
        $mensajeOriginal = "";
        $instruccionUnoVerificada = "";
        $instruccionDosVerificada = "";
        $error = "";

        // Validamos que la solicitud venga con un archivo
        if ($request->hasFile("archivo")){

            //Recibimos el archivo y lo guardamos en la carpeta public/encription/
            $file = $request->file('archivo');    
            $name = "encript_".time().".".$file->guessExtension();
            $path = public_path("encription\\");
            
            // Validamos que el archivo sea de texto
            if($file->guessExtension() == "txt"){

                // Guardamos el archivo
                copy($file, $path.$name);
                // Tratamos el contenido
                $content = file_get_contents($path.$name);
                $content = preg_split("/[\s,]+/", $content);

                $M1 = $content[0]; // Número de caracteres de la instruccion uno
                $M2 = $content[1]; // Número de caracteres de la instruccion dos
                $N = $content[2];  // Número de caracteres del mensaje

                $instruccionUno = $content[3];
                $instruccionDos = $content[4];
                $mensaje = $content[5];                

                // Se valida la regla de longitud de las instrucciones y que corresponda con el largo M1 y M2
                if (strlen($instruccionUno)>=2 && strlen($instruccionUno)<=50 && strlen($instruccionUno)==$M1 && strlen($instruccionDos)>=2 && strlen($instruccionDos)<=50 && strlen($instruccionDos)==$M2){

                    // Se valida la regla de la longitud del mensaje
                    if (strlen($mensaje)>=3 && strlen($mensaje)<=5000 && strlen($mensaje)==$N){

                        // Se valida que el contenido del mensaje contenga los caracteres permitidos [a-zA-Z0-9]
                        if (!preg_match('/[^a-zA-Z0-9]/', $mensaje)){

                            $instruccionUnoSplit = str_split($content[3]);
                            $instruccionDosSplit = str_split($content[4]);
                            $mensajeSplit = str_split($content[5]);

                            // Verifica que la instruccion uno original no tenga dos letras iguales seguidas
                            for ($i = 0; $i <= $M1-1; $i++) {
                                if ($i >= 1) {
                                    if ($instruccionUnoSplit[$i-1] != $instruccionUnoSplit[$i]) {
                                        $instruccionUnoVerificada .= $instruccionUnoSplit[$i];
                                    }
                                }else{
                                    $instruccionUnoVerificada = $instruccionUnoSplit[$i];
                                }     
                            }

                            if ($instruccionUnoVerificada != $instruccionUno){
                                $error = "La instruccion uno no cumple con la regla de no tener dos letras iguales seguidas";
                                return view("welcome")->with('error', $error);  
                            }

                            // Verifica que la instruccion dos original no tenga dos letras iguales seguidas
                            for ($i = 0; $i <= $M2-1; $i++) {
                                if ($i >= 1) {
                                    if ($instruccionDosSplit[$i-1] != $instruccionDosSplit[$i]) {
                                        $instruccionDosVerificada .= $instruccionDosSplit[$i];
                                    }
                                }else{
                                    $instruccionDosVerificada = $instruccionDosSplit[$i];
                                }     
                            }

                            if ($instruccionDosVerificada != $instruccionDos){
                                $error = "La instruccion dos no cumple con la regla de no tener dos letras iguales seguidas";
                                return view("welcome")->with('error', $error);  
                            }                

                            // Reparamos el defecto de la maquina que repite los caracteres del mensaje
                            for ($i = 0; $i <= $N-1; $i++) {

                                if ($i >= 1) {
                                    if ($mensajeSplit[$i-1] != $mensajeSplit[$i]) {
                                        $mensajeOriginal .= $mensajeSplit[$i];
                                    }
                                }else{
                                    $mensajeOriginal = $mensajeSplit[$i];
                                } 

                            }

                            // Verificamos si alguna de las dos instrucciones aparece en el mensaje encriptado
                            if (str_contains($mensajeOriginal, $instruccionUno)) {
                                
                                if (str_contains($mensajeOriginal, $instruccionDos)) { 

                                    // La instruccion uno es igual a la instruccion dos y ambas estan encriptadas en el mensaje
                                    // Esta situación no cumple la regla de "Máximo existe una instrucción escondida por mensaje"
                                    $error = "El mensaje no cumple con la regla de: Máximo existe una instrucción escondida por mensaje";
                                    return view("welcome")->with('error', $error); 

                                }else{ 

                                    // La instruccion uno aparece encriptada en el mensaje
                                    // Escribimos el resultado en el archivo de salida
                                    $fileName = "salida.txt";
                                    $file = fopen($fileName, "w");
                                    fwrite($file, "SI" . PHP_EOL);
                                    fwrite($file, "NO" . PHP_EOL);
                                    fclose($file);
                                    // Volvemos a la vista para mostrar los resultados
                                    return view("welcome")->with('file', $fileName);  
                                      
                                }

                            }else{

                                if (str_contains($mensajeOriginal, $instruccionDos)) {

                                    // La instruccion dos aparece encriptada en el mensaje
                                    // Escribimos el resultado en el archivo de salida
                                    $fileName = "salida.txt";
                                    $file = fopen($fileName, "w");
                                    fwrite($file, "NO" . PHP_EOL);
                                    fwrite($file, "SI" . PHP_EOL);
                                    fclose($file);
                                    // Volvemos a la vista para mostrar los resultados
                                    return view("welcome")->with('file', $fileName);  
                                                     
                                }else{

                                    // Ni la instruccion uno ni la dos aparecen encriptadas en el mensaje
                                    // Escribimos el resultado en el archivo de salida
                                    $fileName = "salida.txt";
                                    $file = fopen($fileName, "w");
                                    fwrite($file, "NO" . PHP_EOL);
                                    fwrite($file, "NO" . PHP_EOL);
                                    fclose($file);
                                    // Volvemos a la vista para mostrar los resultados
                                    return view("welcome")->with('file', $fileName);     
                                }
                                
                            } 

                        }else{
                            $error = "El mensaje no cumple con los caracteres posibles [a-zA-Z0-9]";
                            return view("welcome")->with('error', $error);  
                        }

                    }else{
                        $error = "El mensaje no cumple con el requisito de longitud o no corresponde con el largo de N";
                        return view("welcome")->with('error', $error);      
                    }

                }else{
                    $error = "Una de las instrucciones no cumple con el requisito de longitud o no corresponde al largo de M1 o M2";
                    return view("welcome")->with('error', $error);
                }

            }else{
                $error = "No es un formato de archivo valido .TXT para encriptar";
                return view("welcome")->with('error', $error);
            }

        }else{
            $error = "No selecciono un archivo para procesar";
            return view("welcome")->with('error', $error);
        }
    }
}
